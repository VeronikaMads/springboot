package com.example.springboot.bootstrap;

import com.example.springboot.entity.BookEntity;
import com.example.springboot.repository.BookRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import javax.persistence.Entity;

@Slf4j
@Component
public class BootStrapData implements CommandLineRunner {
    @Autowired
    private BookRepository bookRepository;

    @Override
    public void run(String... args) throws Exception {
        BookEntity book1 = BookEntity.builder()
                .name("Book1")
                .description("Description1")
                .build();
        BookEntity book2 = BookEntity.builder()
                .name("Book2")
                .description("Description2")
                .build();
        BookEntity book3 = BookEntity.builder()
                .name("Book3")
                .description("Description3")
                .build();
        bookRepository.save(book1);
        bookRepository.save(book2);
        bookRepository.save(book3);
        log.info("Books count in DB: {}", bookRepository.count());
    }
}
