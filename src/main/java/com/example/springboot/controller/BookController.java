package com.example.springboot.controller;

import com.example.springboot.entity.BookEntity;
import com.example.springboot.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Controller
public class BookController {
    @Autowired
    private BookRepository bookRepository;
    @GetMapping("/book")
    public String test() {
        List<BookEntity> books = bookRepository.findAll();
        return "Hello!";
    }
}
